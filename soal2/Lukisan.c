#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

void Killer(char source[]);

int main(int argc, char **argv)
{
  if(argc != 2)
  {
    printf("Tolong masukan perintah -a atau -b\n ");
    exit(EXIT_FAILURE);
  }
  
  pid_t pid, sid;
  pid = fork();
  
  if (pid < 0)
    exit(EXIT_FAILURE);

  if (pid > 0)
    exit(EXIT_SUCCESS);

  umask(0);

  sid = setsid();
  if (sid < 0) 
    exit(EXIT_FAILURE);

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  Killer(argv[1]);


  while (1) 
  {
    time_t waktu;
    struct tm* info_waktu;
    
    int giliran;
    pid_t child1_id = fork();
	  
    waktu = time(NULL);
    info_waktu = localtime(&waktu);

    char folder[50];
    strftime(folder, 50, "%Y-%m-%d_%H:%M:%S", info_waktu);

    if (child1_id < 0)
    {
      exit(EXIT_FAILURE); 
    }

    if (child1_id == 0)
    { 
        pid_t ayam =fork();

      if (ayam == 0)
      {
        char *argv[] = {"mkdir", "-p", folder, NULL};
        execv("/bin/mkdir", argv);
      }
      
        while ((wait(&giliran)) > 0);
        for (int i = 1; i <= 15; i++)
        { pid_t ayam2 =fork();
          if (ayam2 == 0)
          {
            time_t waktu;
			      chdir(folder);
            struct tm* file_info_waktu;
        
            waktu = time(NULL);
            file_info_waktu = localtime(&waktu);
        
            char url[50];
            char nama[50];

            sprintf(url, "https://picsum.photos/%d", ((int) waktu % 1000)+50);
            strftime(nama, 50, "%Y-%m-%d_%H:%M:%S", file_info_waktu);
            char *argv[] = {"wget", url, "-qO", nama, NULL};
            execv("/usr/bin/wget", argv);
          }
          
          sleep(5);
        }
        char folder_zip[50];
        sprintf(folder_zip, "%s.zip", folder);
        char *argv[] = {"zip", "-qrm", folder_zip, folder, NULL};
        execv("/usr/bin/zip", argv);
      
    }
    else
      sleep(30);
  }
}

void Killer(char source[])
{
  FILE *Killer;
  Killer = fopen("killer.sh", "w");
  int status;

  if(!strcmp(source, "-a")) {
  fprintf(Killer, "#!/bin/bash\nkill -9 -%d\nrm killer", getpid());}

  if(!strcmp(source, "-b")) {
  fprintf(Killer, "#!/bin/bash\nkill %d\nrm killer", getpid());}

  pid_t parayamkill = fork();

  if(parayamkill == 0)
  {  
    pid_t killayam = fork();

    if (killayam == 0)
    {
      char *argv[] = {"chmod", "u+x", "killer.sh", NULL};
      execv("/bin/chmod", argv);
    }
    else{
      while ((wait(&status)) > 0);
      char *argv[] = {"mv", "killer.sh", "killer", NULL};
      execv("/bin/mv", argv);
    }
  }
  fclose(Killer);
}