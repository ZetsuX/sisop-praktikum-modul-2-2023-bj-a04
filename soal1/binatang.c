#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(){

    pid_t id1, id2, id3, id4, id5;
    int status;

    id1 = fork();

    if (id1 < 0) {
        exit(EXIT_FAILURE);
    }

    else if (id1 == 0) {
        // download file binatang.zip
        printf("Downloading file binatang.zip . . .\n");

        char *argv[] = {"wget", "-q", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "-O", "binatang.zip", NULL};
        execv("/bin/wget", argv);
    }

    else {
        wait(&status);

        id2 = fork();

        if (id2 == 0) {
            // unzip file binatang.zip
            printf("Unzipping binatang.zip . . .\n\n");

            char *argv[] = {"unzip", "-q", "binatang.zip", NULL};
            execv("/bin/unzip", argv);
        }

        else {
            wait(&status);

            // display 1 random animal
            // char *random[] = {"shuf -en 1 *.jpg | cut -d . -f 1"};
            system("echo Grape-Kun akan menjaga hewan: ");
            system("shuf -en 1 *.jpg | cut -d . -f 1");

            // make HewanDarat, HewanAmphibi, and HewanAir directory 
            // then move each animal to its directory
            system("mkdir -p HewanDarat HewanAmphibi HewanAir");
            system("mv *darat* HewanDarat/");
            system("mv *amphibi* HewanAmphibi/");
            system("mv *air* HewanAir/");

            id3 = fork();

            if (id3 == 0) {
                // zip directory HewanDarat
                printf("\nZipping HewanDarat directory . . .\n");

                char *argv[] = {"zip", "-r", "-q", "HewanDarat.zip", "HewanDarat/", NULL};
                execv("/bin/zip", argv);
            }

            else {               
                wait(&status);

                id4 = fork();

                if (id4==0) {

                    // zip directory HewanAmphibi
                    printf("Zipping HewanAmphibi directory . . .\n");

                    char *argv[] = {"zip", "-r", "-q", "HewanAmphibi.zip", "HewanAmphibi/", NULL};
                    execv("/bin/zip", argv);
                }

                else {
                    wait(&status);

                    id5 = fork();

                    if (id5 == 0) {
                        // zip directory HewanAir
                        printf("Zipping HewanAir directory . . .\n");
                        
                        char *argv[] = {"zip", "-r", "-q", "HewanAir.zip", "HewanAir/", NULL};
                        execv("/bin/zip", argv);
                    }

                    else {
                        wait(&status);
                        
                        // delete HewanDarat, HewanAmphibi, and HewanAir directory
                        printf("Deleting HewanDarat, HewanAmphibi, and HewanAir directory\n");

                        system("rm -r HewanDarat/ HewanAmphibi/ HewanAir/");
                    }
                }
            }
        }
    }
}