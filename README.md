# Praktikum Sistem Operasi 2003 Modul 2 (Kelompok A04)

## Daftar Isi

-   [Anggota Kelompok A04](#anggota-kelompok)
-   [Permasalahan 1](#permasalahan-1)
-   [Permasalahan 2](#permasalahan-2)
-   [Permasalahan 3](#permasalahan-3)
-   [Permasalahan 4](#permasalahan-4)

## Anggota Kelompok

-   Widian Sasi Disertasiani / 5025211024
-   Kevin Nathanael Halim / 5025211140
-   Andhika Lingga Mariano / 5025211161

## Permasalahan 1

### Latar Belakang
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 

### Keterangan Permasalahan
- Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.
- Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.
- Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.
- Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

Catatan :
- Untuk melakukan zip dan unzip tidak boleh menggunakan system

### Tujuan Permasalahan
Membuat program sesuai dengan ketentuan pada soal yang dapat mendownload file, melakukan unzip, memilih satu file secara acak, memfilter tiap file ke dalam direktori yang sesuai, dan melakukan zip.

### Konsep Penyelesaian
- Mendownload file pada url yang disediakan.
- Melakukan unzip pada file zip yang telah didownload.
- Menampilkan nama dari salah satu file yang telah didownload pada console.
- Membuat 3 buah direktori **HewanDarat**, **HewanAmphibi**, dan **HewanAir**.
- Memindahkan setiap file ke dalam direktori yang sesuai.
- Melakukan zip pada ketiga direktori tersebut.
- Menghapus direktori **HewanDarat**, **HewanAmphibi**, dan **HewanAir**.

### Implementasi dan Penjelasan (Kode)
1. Import library yang diperlukan.
    ```c
    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <sys/wait.h>
    ```
2. Deklarasi id sebagai pid_t dan juga status sebagai integer, kemudian membuat beberapa process.
    ```c
    int main() {
        pid_t id1, id2, id3, id4, id5;
        int status;
    ```

    - Pada process pertama, child process akan mendownload file **binatang.zip** dari url pada soal dengan menggunakan command **wget**. Sementara parent process akan membuat process baru.
        ```c
        if (id1 < 0) {
            exit(EXIT_FAILURE);
        }

        else if (id1 == 0) {
            // download file binatang.zip
            printf("Downloading file binatang.zip . . .\n");

            char *argv[] = {"wget", "-q", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "-O", "binatang.zip", NULL};
            execv("/bin/wget", argv);
        }

        else {
            wait(&status);

            id2 = fork();
        ```

    - Pada process kedua, child process akan melakukan unzip terhadap file **binatang.zip** dengan menggunakan command **unzip**. Sementara parent process akan menampilkan salah satu nama hewan dengan command **shuf** dan **cut**, membuat direktori **HewanDarat**, **HewanAmphibi**, dan **HewanAir** dengan command **mkdir**, serta memindahkan file-file yang ada ke dalam direktorinya masing-masing dengan command **mv**. Setelah itu, parent process akan membuat process baru.
        ```c
        if (id2 == 0) {
            // unzip file binatang.zip
            printf("Unzipping binatang.zip . . .\n\n");

            char *argv[] = {"unzip", "-q", "binatang.zip", NULL};
            execv("/bin/unzip", argv);
        }

        else {
            wait(&status);

            // display 1 random animal
            system("echo Grape-Kun akan menjaga hewan: ");
            system("shuf -en 1 *.jpg | cut -d . -f 1");

            // make HewanDarat, HewanAmphibi, and HewanAir directory 
            // then move each animal to its directory
            system("mkdir -p HewanDarat HewanAmphibi HewanAir");
            system("mv *darat* HewanDarat/");
            system("mv *amphibi* HewanAmphibi/");
            system("mv *air* HewanAir/");

            id3 = fork();
        ```

    - Pada process ketiga, child process akan melakukan zip terhadap folder **HewanDarat** den. Sementara parent process akan membuat folder baru.
        ```c
        if (id3 == 0) {
                // zip directory HewanDarat
                printf("\nZipping HewanDarat directory . . .\n");

                char *argv[] = {"zip", "-r", "-q", "HewanDarat.zip", "HewanDarat/", NULL};
                execv("/bin/zip", argv);
            }

            else {               
                wait(&status);

                id4 = fork();
        ``` 

    - Pada process keempat, child process akan melakukan zip terhadap folder **HewanAmphibi**. Sementara parent process akan membuat folder baru. 
        ```c
        if (id4 == 0) {

                    // zip directory HewanAmphibi
                    printf("Zipping HewanAmphibi directory . . .\n");

                    char *argv[] = {"zip", "-r", "-q", "HewanAmphibi.zip", "HewanAmphibi/", NULL};
                    execv("/bin/zip", argv);
                }

                else {
                    wait(&status);

                    id5 = fork();
        ```
    
    - Pada process terakhir, child process akan melakukan zip terhadap folder **HewanAir**. Sementara parent process akan menghapus ketiga folder **HewanDarat**, **HewanAmphibi**, dan **HewanAir**.
        ```c
        if (id5 == 0) {
            // zip directory HewanAir
            printf("Zipping HewanAir directory . . .\n");
                                
            char *argv[] = {"zip", "-r", "-q", "HewanAir.zip", "HewanAir/", NULL};
            execv("/bin/zip", argv);
        }

        else {
            wait(&status);
                                
            // delete HewanDarat, HewanAmphibi, and HewanAir directory
            printf("Deleting HewanDarat, HewanAmphibi, and HewanAir directory\n");

            system("rm -r HewanDarat/ HewanAmphibi/ HewanAir/");
        }
        ```

### Output
- Program dijalankan <br>
    <img src="https://cdn.discordapp.com/attachments/1088449784307777597/1091291403046371358/image.png">

- Isi file HewanDarat.zip <br>
    <img src="https://cdn.discordapp.com/attachments/1088449784307777597/1091291959575977994/image.png">

- Isi file HewanAmphibi.zip <br>
    <img src="https://cdn.discordapp.com/attachments/1088449784307777597/1091291959760519270/image.png">

- Isi file HewanAir.zip <br>
    <img src="https://cdn.discordapp.com/attachments/1088449784307777597/1091291959945080913/image.png">

### Kendala yang sempat dihadapi
- Ketika ingin menampilkan nama hewan menggunakan command **shuf**, nama hewan tidak muncul pada console. 

## Permasalahan 2

### Latar Belakang 
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya.Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi.

### Keterangan Permasalahan 
- Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].
- Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
- Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).
- Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
- Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

### Catatan:
- Tidak boleh menggunakan system()
- Proses berjalan secara daemon
- Proses download gambar pada beberapa folder dapat 
- berjalan secara bersamaan (overlapping)

### Tujuan Permasalahan 
Membuat setiap 30 detik folder yang di dalamnya berisikan foto dari link yang diberikan sebanyak 15 foto, yang kemudian folder tersebut akan dizip lalu dihapus, untuk menghentikan proses pada program, saya harus membuat file killer yang memiliki 2 format, yaitu a untuk mengentikan semua proses yang berlangsung, dan b untuk menghentikan parent proses.

### Implementasi Kode
```c 
void Killer(char source[]);

int main(int argc, char **argv)
{
  if(argc != 2)
  {
    printf("Tolong masukan perintah -a atau -b\n ");
    exit(EXIT_FAILURE);
  }
  
  pid_t pid, sid;
  pid = fork();
  
  if (pid < 0)
    exit(EXIT_FAILURE);

  if (pid > 0)
    exit(EXIT_SUCCESS);

  umask(0);

  sid = setsid();
  if (sid < 0) 
    exit(EXIT_FAILURE);

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  Killer(argv[1]);
  ```
  Pertama yang saya lakukan adalah memanggil fungsi killer, kemudian membuat sebuah kondisi apabila ketika kita menjalankan file tapi tidak sama dengan 2 kata, maka akan muncul perintah "Tolong masukan perintah -a atau -b". Lalu saya panggil killer, agar perintah tersebut bisa diakses. 

- Membuat Progam untuk membuat, mendownload, mengzip serta menghapus file 

  ```c
  time_t waktu;
    struct tm* info_waktu;
    
    int giliran;
    pid_t child1_id = fork();
	  
    waktu = time(NULL);
    info_waktu = localtime(&waktu);

    char folder[50];
    strftime(folder, 50, "%Y-%m-%d_%H:%M:%S", info_waktu);
```
Lalu, mendeklarasikan time_t ke variable waktu, serta struct tm ke info_waktu, yang dimana waktu = time dan info_waktu = local time. Setelah itu saya membuat char folder yang nantinya akan digunakan sebagai nama folder, dengan format YY-MM-DD_H_M_s yang diambil dari local time. 

```c
if (ayam == 0)
      {
        char *argv[] = {"mkdir", "-p", folder, NULL};
        execv("/bin/mkdir", argv);
```
Membuat kondisi untuk membuat folder baru dengan format nama char folder.

```c
while ((wait(&giliran)) > 0);
        for (int i = 1; i <= 15; i++)
        { pid_t ayam2 =fork();
          if (ayam2 == 0)
          {
            time_t waktu;
			      chdir(folder);
            struct tm* file_info_waktu;
        
            waktu = time(NULL);
            file_info_waktu = localtime(&waktu);
        
            char url[50];
            char nama[50];

            sprintf(url, "https://picsum.photos/%d", ((int) waktu % 1000)+50);
            strftime(nama, 50, "%Y-%m-%d_%H:%M:%S", file_info_waktu);
            char *argv[] = {"wget", url, "-qO", nama, NULL};
            execv("/usr/bin/wget", argv);
          }
          
          sleep(5);
```
Melakukan looping untuk mendownload gambar dari picsum sebanyak 15 kali, dengan ukuran t%1000+50, serta format nama YY-MM-DD_H:M:S. Pendownloadan gambar tersebut akan dilakukan tiap 5 detik. 

```c
char folder_zip[50];
        sprintf(folder_zip, "%s.zip", folder);
        char *argv[] = {"zip", "-qrm", folder_zip, folder, NULL};
        execv("/usr/bin/zip", argv);
```
Merupakan program untuk melakukan zip dan menghapus folder yang sudah berisi 15 foto. 
```c
 else
      sleep(30);
```
Menandakan pembuatan folder akan dilangsungkan setiap 30 detik. 
- Membuat fungsi killer untuk menghentikan proses pada program 
```c
void Killer(char source[])
{
  FILE *Killer;
  Killer = fopen("killer.sh", "w");
  int status;

  if(!strcmp(source, "-a")) {
  fprintf(Killer, "#!/bin/bash\nkill -9 -%d\nrm killer", getpid());}

  if(!strcmp(source, "-b")) {
  fprintf(Killer, "#!/bin/bash\nkill %d\nrm killer", getpid());}
```
Membuat kondisi apabila input "-a" maka ia akan menghentikan keseluruhan proses yang sedang berlangsung. Apablia "-b", maka ia hanya akan menghentikan proses pembuatann file, namun download serta zip akan terus berlangsung hingga selesai. 

```c
 if(parayamkill == 0)
  {  
    pid_t killayam = fork();

    if (killayam == 0)
    {
      char *argv[] = {"chmod", "u+x", "killer.sh", NULL};
      execv("/bin/chmod", argv);
    }
    else{
      while ((wait(&status)) > 0);
      char *argv[] = {"mv", "killer.sh", "killer", NULL};
      execv("/bin/mv", argv);
    }
  }
  fclose(Killer);
}
```
Membuat kondisi apabila child proses ayam = 0, maka killer.sh dapat mengexecute program. lalu killer.sh akan dipasing ke killer 

- Hasil akhir dari program 
<img src ="https://cdn.discordapp.com/attachments/861224477966991371/1093507840586547251/WhatsApp_Image_2023-04-06_at_19.08.13.jpg">

- Hambatan
Ketika input perintah, itu bisa apa saja asalkan 2 kata, apabila saya input -c tetap akan bisa dijalankan. 

## Permasalahan 3

### Latar Belakang
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut **hanya dengan 1 Program C** bernama **“filter.c”**

### Keterangan Permasalahan
- Pertama-tama, Program filter.c akan **mengunduh** file yang berisikan <a href="https://drive.google.com/file/d/1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF/view?usp=share_link">database para pemain bola</a>. Kemudian dalam program yang sama diminta dapat melakukan **extract** “players.zip”. Lalu **hapus** file zip tersebut agar tidak memenuhi komputer Ten Hag.
- Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang **bukan** dari Manchester United yang ada di directory.  
- Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan **posisi** mereka dalam **waktu bersamaan dengan 4 proses yang berbeda**. Untuk kategori folder akan menjadi 4 yaitu **Kiper, Bek, Gelandang, dan Penyerang**.
- Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan **Kesebelasan Terbaik** untuk menjadi senjata utama MU berdasarkan **rating** terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi **Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt** dan akan ditaruh di /home/[users]/

Catatan :
- Format nama file yang akan diunduh dalam zip dan isi txt formasi berupa [nama]_[tim]_[posisi]_[rating].png
- Tidak boleh menggunakan system()
- Tidak boleh memakai function C mkdir() ataupun rename().
- Gunakan exec() dan fork().
- Directory “.” dan “..” tidak termasuk yang akan dihapus.
- Untuk poin d **DIWAJIBKAN** membuat fungsi bernama **buatTim(int, int, int)**, dengan input 3 value integer dengan urutan bek, gelandang, dan striker.

### Tujuan Permasalahan
Membuat sebuah program yang dapat mendownload, mengextract, lalu menghapus file zip, memilah beberapa file tertentu dan menghapus file yang tidak diperlukan, memfilter file-file tersebut ke dalam direktori yang sesuai, serta mengelompokkan file ke dalam sebuah file txt.

### Konsep Penyelesaian
- Mendownload file melalui url yang disediakan.
- Melakukan extract pada file zip yang telah didownload kemudian menghapus file zip tersebut.
- Menghapus semua file yang tidak memuat string "ManUtd" pada nama filenya.
- Menyalin semua file ke dalam direktori **Kiper**, **Bek**, **Gelandang**, dan **Penyerang** kemudian menghapus file yang tidak sesuai pada tiap direktori agar diperoleh direktori yang berisi file yang sesuai.
- Meminta input dari user mengenai jumlah pemain dalam setiap kategori yang ingin dimasukkan ke dalam tim.
- Memanggil fungsi **buatTim(bek, gelandang, striker)** untuk membuat kesebelasan terbaik yang akan dimasukkan ke dalam file txt.

### Implementasi dan Penjelasan (Kode)
1. Import library yang diperlukan.
    ```c
    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <sys/wait.h>
    ```

2. Membuat fungsi **buatTim()** yang menerima tiga buah input, yaitu jumlah pemain bek, gelandang, dan striker.
    - Pertama, dilakukan deklarasi variabel id sebagai pid_t, status sebagai integer, dan juga array pada tiap kategori untuk menyimpan command yang nantinya akan dijalankan.
        ```c
        void buatTim(int bek, int gelandang, int striker) {

            pid_t id1, id2, id3;
            int status;
            char kiperStr[200], bekStr[200], gelStr[200], strikerStr[200];
        ```

    - Kemudian digunakan fungsi **sprintf()** untuk menyimpan command-command yang akan dipanggil.
        ```c
        sprintf(kiperStr, "ls Kiper/ | sort -r -t _ -k 4 | head -n 1 | cut -d . -f 1 > /home/$USER/Formasi_%d-%d-%d.txt", bek, gelandang, striker);

        sprintf(bekStr, "ls Bek/ | sort -r -t _ -k 4 | head -n %d | cut -d . -f 1 >> /home/$USER/Formasi_%d-%d-%d.txt", bek, bek, gelandang, striker);

        sprintf(gelStr, "ls Gelandang/ | sort -r -t _ -k 4 | head -n %d | cut -d . -f 1 >> /home/$USER/Formasi_%d-%d-%d.txt", gelandang, bek, gelandang, striker);

        sprintf(strikerStr, "ls Penyerang/ | sort -r -t _ -k 4 | head -n %d | cut -d . -f 1 >> /home/$USER/Formasi_%d-%d-%d.txt", striker, bek, gelandang, striker);
        ```
        `ls` : digunakan untuk mengambil semua nama file dalam folder.<br>
        `sort  -r -t _ -k 4` : digunakan untuk mengurutkan file berdasarkan rating pemain mulai dari yang terbesar.<br>
        `head -n %d` : digunakan untuk mengambil beberapa nama file teratas.<br>
        `cut -d . -f 1` : digunakan untuk menghapus ekstensi .png dari nama file yang diambil.<br>
        `>> /home/$USER/Formasi_%d-%d-%d.txt` : digunakan untuk memasukkan nama-nama pemain tersebut ke dalam file .txt dalam direktori /home/user/ sesuai dengan jumlah pemain tiap kategori.

    - Process baru dibuat, di mana child process akan menjalankan command dalam array kiperStr[] untuk menambahkan kiper ke dalam file .txt. Sementara parent process akan membuat process baru.
        ```c
        id1 = fork();

        if (id1 < 0) {
            exit(EXIT_FAILURE);
        }

        else if (id1 == 0) {
            // add kiper player to formasi txt
            execlp("/bin/sh", "sh", "-c", kiperStr, NULL);
        }

        else {
            wait(&status);
            
            id2 = fork();
        ```

    - Pada process kedua ini, child process akan menjalankan command dalam array bekStr[] untuk menambahkan bek ke dalam file .txt. Sementara parent process akan membuat process baru. 
        ```c
        if (id2 == 0) {
                // sort and add bek players to formasi txt
                execlp("/bin/sh", "sh", "-c", bekStr, NULL);
            }

            else {
                wait(&status);

                id3 = fork();
        ```

    - Pada process ketiga, child process akan menjalankan command dalam array gelStr[] untuk menambahkan gelandang ke dalam file .txt. Sementara parent process akan menjalankan command dalam array strikerStr[] untuk menambahkan penyerang ke dalam file .txt.
        ```c
            if (id3 == 0) {
                        // sort and add gelandang players to formasi txt
                        execlp("/bin/sh", "sh", "-c", gelStr, NULL);
                    }

                    else {
                        wait(&status);
                        

                        // sort and add striker players to formasi txt
                        execlp("/bin/sh", "sh", "-c", strikerStr, NULL);
                    }
                }
            }
        }
        ```

3. Pada fungsi main(), dideklarasikan variabel id sebagai pid_t dan status sebagai integer. Setelah itu program membuat beberapa process.

    ```c
    int main() {
        pid_t id0, id1, id2, id3, id4, id5, id6;
        int status;
    ```

    - Pada process pertama, child process akan membuat sebuah process baru. Sementara parent process akan menunggu hingga semua perintah dalam child process selesai dikerjakan.
        ```c
        id0 = fork();

        if(id0 < 0) {
            exit(EXIT_FAILURE);
        }

        else if(id0 == 0){
        
            id1 = fork();

        ```

    - Pada process kedua, child process akan mendownload file **player.zip** menggunakan command **wget**. Sementara parent process akan membuat process baru.
        ```c
        if (id1 < 0) {
            exit(EXIT_FAILURE);
        }

        else if (id1 == 0) {
            // download file player.zip
            printf("Downloading player.zip . . .\n");

            char *argv[] = {"wget", "-q", "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", "-O", "player.zip", NULL};
            execv("/bin/wget", argv);
        }

        else {
            wait(&status);

            id2 = fork();
        ```

    - Pada process ketiga, child process akan melakukan unzip terhadap file **player.zip** menggunakan command **unzip**. Sementara parent process akan membuat process baru.
        ```c
        if (id2 == 0) {
            // extract player.zip
            printf("Unzipping player.zip\n");

            char *argv[] = {"unzip", "-q", "player.zip", NULL};
            execv("/bin/unzip", argv);
        }

        else {
            wait(&status);
            id3 = fork();
        ```

    - Pada process keempat, child process akan menghapus file **player.zip** menggunakan command **rm**. Sementara parent process akan membuat process baru.
        ```c
        if(id3 == 0){
            // delete player.zip
            printf("Deleting player.zip\n");

            char *argv[] = {"rm", "player.zip", NULL};
            execv("/bin/rm", argv);
        }

        else {
            wait(&status);

            id4 = fork();
        ```
    
    - Pada process kelima, child process akan mencari file-file yang tidak memuat "ManUtd" pada nama filenyya dan kemudian dihapus menggunakan command **find**.
        ```c
        if (id4 == 0) {
            // delete non Manchester United players
            printf("Deleting non Manchester United players\n");

            char *argv[] = {"find", "players/", "-type", "f", "-not", "-name", "*ManUtd*", "-delete", NULL};
            execv("/bin/find", argv);
        }
        ```

    - Dalam parent process ini, terdapat dua buah array **folder[]** dan **folder2[]** yang digunakan untuk menyimpan nama folder tiap kategori. Process baru akan dibuat di dalam for loop, di mana child process akan menyalin semua file pada direktori **players** ke dalam direktori **Kiper**, **Bek**, **Gelandang**, dan **Penyerang**.
        ```c
        else {
            wait(&status);

            char *folder[]={"Kiper", "Bek", "Gelandang", "Penyerang"};

            for(int i=0; i<4; i++){
                id5 = fork();

                if(id5 == 0){
                    // copy players folder to each folder (Kiper, Bek, Gelandang, and Penyerang)
                    char *argv[] = {"cp", "-R", "players/", folder[i], NULL};
                    execv("/bin/cp", argv);
                }
            }
        ``` 

    - Kemudian, parent processnya akan memanggil for loop untuk mencari dan menghapus semua file yang tidak sesuai dengan direktorinya.
        ```c
        if(id5 != 0) {
            wait(&status);

            char *folder2[] = {"*Kiper*", "*Bek*", "*Gelandang*", "*Penyerang*"};

            for(int i=0; i<4; i++){
                id6 = fork();

                if(id6 == 0){
                    // filter players according to their folder
                    char *argv[] = {"find", folder[i], "-type", "f", "-not", "-name", folder2[i], "-delete", NULL};
                    execv("/bin/find", argv);
                }
            }
        }
        ```

    - Setelah seluruh perintah di atas dijalankan, parent dari process pertama akan masuk ke dalam while loop untuk meminta input dari user berupa jumlah pemain bek, gelandang, dan striker yang ingin dibentuk. Apabila formasi yang dibentuk setelah ditambah seorang kiper tidak tepat beranggotakan 11 orang, maka program akan meminta user untuk memasukkan ulang jumlah pemain. Sedangkan jika formasi yang dibentuk tepat beranggotakan 11 orang, maka program akan keluar dari while loop dan memanggil fungsi **buatTim()**.
        ```c
            else {
            wait(&status);

            int bek, gelandang, striker, count=1;

            while(count) {
                printf("\nMasukkan formasi pemain yang diinginkan: \n");
                printf("Bek: ");
                scanf("%d", &bek);
                printf("Gelandang: ");
                scanf("%d", &gelandang);
                printf("Striker: ");
                scanf("%d", &striker);

                if (bek == 0 || gelandang == 0 || striker == 0) {
                    printf("Jumlah pemain tidak boleh 0! Silahkan coba lagi\n");
                }

                else if (bek + gelandang + striker < 10) {
                    printf("Jumlah pemain kurang dari 11! Silahkan coba lagi\n");
                }

                else if (bek + gelandang + striker > 10) {
                    printf("Jumlah pemain lebih dari 11! Silahkan coba lagi\n");
                }

                else {
                    count = 0;
                }

            }
            printf("File Formasi_%d-%d-%d.txt telah dibuat di directory /home/%s/\n", bek, gelandang, striker, getlogin());


            buatTim(bek, gelandang, striker);
        }
        ```

### Output
- Contoh input program<br>
<img src="https://cdn.discordapp.com/attachments/1088449784307777597/1091325371196125305/image.png">

    Program di atas akan membuat file Formasi_3-4-3.txt pada directory /home/user/.

- Isi direktori players<br>
<img src="https://cdn.discordapp.com/attachments/1088449784307777597/1091325561558810744/image.png">

- Isi direktori Kiper<br>
<img src="https://cdn.discordapp.com/attachments/1088449784307777597/1091325647089041449/image.png">

- Isi direktori Bek<br>
<img src="https://cdn.discordapp.com/attachments/1088449784307777597/1091325731222601780/image.png">

- Isi direktori Gelandang<br>
<img src="https://cdn.discordapp.com/attachments/1088449784307777597/1091325810176184330/image.png">

- Isi direktori Penyerang<br>
<img src="https://cdn.discordapp.com/attachments/1088449784307777597/1091325916677943306/image.png">

- Isi file Formasi_3-4-3.txt<br>
<img src="https://cdn.discordapp.com/attachments/1088449784307777597/1091326260954812446/image.png">


### Kendala yang sempat dihadapi
- Proses pengkategorian tiap file ke dalam direktori yang sesuai pada awalnya mengalami kendala karena tidak dapat menggunakan command **mv** dengan wildcard(*) dalam fungsi **execv()**.
- Dalam fungsi buatTim(), proses pengambilan pemain dari rating tertinggi sempat tidak mengeluarkan hasil yang diinginkan.

## Permasalahan 4

### Latar Belakang

Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat **program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C** karena baru dipelajari olehnya.

### Keterangan Permasalahan

-   Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
-   Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
-   Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan "error" apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.
-   Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.
-   Bonus poin apabila CPU state minimum.

Contoh masukkan untuk run :
`/program \* 44 5 /home/Banabil/programcron.sh`

### Tujuan Permasalahan

Membuat program menyerupai croontab yang dapat menjalankan script bash menggunakan bahasa C sesuai dengan keterangan yang tertera.

### Konsep Penyelesaian

-   Menerima input berupa waktu dan juga file path melalui argumen dan melakukan validasi terhadap keseluruhannya dengan mengeluarkan error message yang sesuai apabila terdapat error
-   Membuat Daemon Process
-   Mendapatkan waktu terkini
-   Melakukan branching sesuai dengan kondisi input jam, menit, dan detik yang diterima (terdapat 2^3 = 8 kemungkinan)
-   Melakukan sleep dalam jumlah waktu tertentu hingga mencapai interval terdekat yang sesuai dengan masukkan
-   Menjalankan script bash dengan membuat child dan menggunakan exec
-   Kembali melakukan sleep hingga interval berikutnya

Keterangan :<br>
Harapannya dengan menggunakan sleep saat tidak bekerja, CPU state yang digunakan oleh program bernilai minimum karena tidak harus melakukan pengecekkan waktu setiap detik (sebelumnya cara yang sempat terpikirkan juga adalah dengan melakukan pengecekkan jam, menit, dan detik setiap detik, kemudian menjalankan script bash setiap kali ketiganya sesuai yakni angkanya sama atau berupa '\*')

### Implementasi dan Penjelasan (Kode)

1. Import library yang dibutuhkan dan juga define MAX untuk mewakili size maksimal serta STAR untuk mewakili nilai '\*' / asterisk. Tak lupa juga dibuat variabel global untuk menampung path file dan argumen untuk eksekusi fungsi execv()

    ```c
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <fcntl.h>
    #include <errno.h>
    #include <unistd.h>
    #include <syslog.h>
    #include <string.h>
    #include <time.h>
    #include <ctype.h>

    #define MAX 10001
    #define STAR 404

    char path[MAX];
    char *args[] = {"bash", path, NULL};
    ```

2. Dibuat fungsi-fungsi utility untuk keperluan-keperluan berikutnya untuk sedikit menghindari prosedural dengan fungsi sebagai berikut,

    - validateTime() berfungsi untuk melakukan validasi terhadap waktu yang diterima sebagai input dari argumen

        ```c
        short validateTime(char* str, short max, char* name) {
            if (strlen(str) <= 0 || strlen(str) > 2) {
                printf("The entered %s is invalid! Please check and rerun the program with the correct %s.\n", name, name);
                return -1;
            }

            if (str[0] == '*') {
                if (strlen(str) == 1) {
                    return STAR;
                } else {
                    printf("The entered %s is invalid! Please check and rerun the program with the correct %s.\n", name, name);
                    return -1;
                }
            }

            if (strlen(str) == 2) {
                if (!isdigit(str[0]) || !isdigit(str[1])) {
                    printf("The entered %s is invalid! Please check and rerun the program with the correct %s.\n", name, name);
                    return -1;
                }
            } else {
                if (!isdigit(str[0])) {
                    printf("The entered %s is invalid! Please check and rerun the program with the correct %s.\n", name, name);
                    return -1;
                }
            }

            int t = atoi(str);
            if (t < 0 || t > max) {
                printf("The entered %s is outside the range of 0 - %hi! Please check and rerun the program with the correct %s.\n", name, max, name);
                return -1;
            }

            return t;
        }
        ```

    - getDiff() berfungsi untuk mendapatkan perbedaan waktu dari waktu terkini hingga ke interval yang telah dimasukkan sebagai parameter

        ```c
        int getDiff(int cur, int t, int add, int mul, int subt) {
            if (cur < t) {
                return ((t - cur)*mul);
            } else if (cur > t) {
                return (((t + add - cur)*mul) - subt);
            }

            return 0;
        }
        ```

    - execCmd() berfungsi untuk membuat child process dan menjalankan fungsi execv() untuk menjalankan script bash melalui variabel global args

        ```c
        void execCmd() {
            pid_t chId;
            chId = fork();

            if (chId < 0) {
                exit(EXIT_FAILURE);
            }

            if (chId == 0) {
                execv("/bin/bash", args);
            }
        }
        ```

    - iterTask() berfungsi untuk menjalankan fungsi execCmd secara berulang kali dengan delay tertentu dalam jumlah dan juga delay sesuai dengan parameter yang diberikan

        ```c
        void iterTask(int max, int slp) {
            execCmd();
            for (int i = 1 ; i < max ; i++) {
                sleep(slp);
                execCmd();
            }
        }
        ```

3. Dilakukan validasi mulai dari pengecekkan jumlah argumen dari variabel argc, pengecekkan argumen-argumen waktu dengan fungsi validateTime() yang telah disebutkan sebelumnya, hingga pengecekan dan pengambilan file path yang merupakan argumen terakhir.

    ```c
    if (argc != 5) {
        printf("The entered arguments are invalid! Please check and rerun the program with the fixed arguments.\n");
        return 0;
    }

    short h, m, s;

    h = validateTime(argv[1], 23, "hour");
    if (h == -1) return 0;

    m = validateTime(argv[2], 59, "minute");
    if (m == -1) return 0;

    s = validateTime(argv[3], 59, "second");
    if (s == -1) return 0;

    if (strlen(argv[4]) > MAX) {
        printf("The entered file path is too long. Please shorten it and then rerun the program with the shortened path.\n");
        return 0;
    }

    strcpy(args[1], argv[4]);
    ```

4. Dibuat sebuah Daemon Process dengan melakukan fork kemudian memberikan SID kepada child process yang ada dan dilanjutkan dengan penggantian file permission menggunakan umask dan mematikan parent process.

    ```c
    pid_t pid, sid;
    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    printf("Daemon Process created successfully! (PID : %d)\n", sid);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    ```

5. Diambil jam, menit, dan detik terkini menggunakan fungsi dan properties dari library time.h.

    ```c
    time_t nowTime = time(NULL);
    struct tm *tm_struct = localtime(&nowTime);

    int curh = tm_struct->tm_hour;
    int curm = tm_struct->tm_min;
    int curs = tm_struct->tm_sec;
    ```

6. Dilakukan branching untuk 8 kondisi input jam, menit, dan detik yang memungkinkan untuk kemudian dilakukan perulangan eksekusi tanpa henti sesuai dengan waktu yang diberikan. Branching yang dilakukan sebagai berikut,

    - Apabila input berupa `\* \* \*`, maka script bash akan dijalankan setiap detik sehingga hanya perlu dilakukan sleep selama 1 detik sebagai jeda antar execCmd()

        ```c
        if (h == STAR && m == STAR && s == STAR) {
            while (1) {
                execCmd();
                sleep(1);
            }
        }
        ```

    - Apabila input berupa `\* \* S`, maka script bash akan dijalankan apabila detik terkini adalah S. Untuk itu, akan dilakukan sleep hingga mencapai detik S terdekat dari waktu terkini, kemudian script bash akan dijalankan sebelum diberi jeda 60 detik agar berikutnya program berjalan setiap menit di detik ke-S

        ```c
        else if (h == STAR && m == STAR && s != STAR) {
            int incr = 0;
            incr += getDiff(curs, s, 60, 1, 0);

            sleep(incr);
            execCmd();

            while (1) {
                sleep(60);
                execCmd();
            }
        }
        ```

    - Apabila input berupa `\* M \*`, maka script bash akan dijalankan apabila menit terkini adalah M. Untuk itu, akan diawali dengan sleep hingga menit M yang terdekat dari waktu terkini. Kemudian dilanjutkan dengan menjalankan script bash setiap detik selama menit M dan kemudian diberikan jeda 59 menit sebelum kembali menjalankan script bash lagi

        ```c
        else if (h == STAR && m != STAR && s == STAR) {
            int incr = 0;
            incr += getDiff(curm, m, 60, 60, 0);
            incr += getDiff(curs, 0, 60, 1, 60);

            sleep(incr);

            while (1) {
                iterTask(60, 1);
                sleep(3540);
            }

        }
        ```

    - Apabila input berupa `H \* \*`, maka script bash akan dijalankan apabila jam terkini adalah H. Untuk itu, akan dilakukan pengecekkan terlebih dahulu apakah jam terkini sudah sama atau belum. Bila sudah sama, maka langsung saja dijalankan script bash terus menerus hingga jam berganti. Bila belum, maka akan dilakukan sleep hingga tercapai jam tersebut di menit dan detik ke-0. Setelah itu, branch akan kembali menjadi satu dimana akan dilakukan sleep selama 23 jam sebagai jeda antar setiap iterasi eksekusi

        ```c
        else if (h != STAR && m == STAR && s == STAR) {
            int incr = 0;
            if (h == curh) {
                incr += getDiff(curm, 59, 60, 60, 0);
                incr += getDiff(curs, 59, 60, 1, 60);

                iterTask(incr + 1, 1);
                sleep(82800);

            } else {
                incr += getDiff(curh, h, 24, 3600, 0);
                incr += getDiff(curm, 0, 60, 60, 3600);
                incr += getDiff(curs, 0, 60, 1, 60);

                sleep(incr);
            }

            while (1) {
                iterTask(3600, 1);
                sleep(82800);
            }
        }
        ```

    - Apabila input berupa `\* M S`, maka script bash akan dijalankan apabila menit terkini adalah M dan detiknya adalah S. Untuk itu, akan dilakukan sleep hingga menit ke-M dan detik ke-S yang terdekat sebelum script bash dijalankan. Kemudian, untuk seterusnya diberikan jeda 1 jam sebelum kembali melakukan eksekusi

        ```c
        else if (h == STAR && m != STAR && s != STAR) {
            int incr = 0;
            incr += getDiff(curm, m, 60, 60, 0);
            incr += getDiff(curs, s, 60, 1, 60);

            sleep(incr);

            while(1) {
                execCmd();
                sleep(3600);
            }

        }
        ```

    - Apabila input berupa `H \* S`, maka script akan dijalankan apabila jam terkini adalah H dengan detiknya adalah S. Untuk itu, akan dilakukan pengecekkan apakah jam terkini sudah sama dengan H atau belum. Bila sudah, maka iterasi eksekusi script bash dengan jeda semenit per eksekusi akan dilakukan hingga jam berganti. Bila belum, maka akan dilakukan sleep hingga jam ke-H dan detik ke-S yang terdekat. Setelah itu, kedua branch akan bergabung dan akan dilakukan iterasi eksekusi yang sama dengan jeda 23 jam per iterasi

        ```c
        else if (h != STAR && m == STAR && s != STAR) {
            int incr = 0;
            if (h == curh) {
                if (curm < 59 || (curm == 59 && curs <= s)) {
                    incr += getDiff(curs, s, 60, 1, 0);
                    sleep(incr);

                    iterTask(60 - curm, 60);
                    sleep(82800);
                }
            } else {
                incr += getDiff(curh, h, 24, 3600, 0);
                incr += getDiff(curm, 0, 60, 60, 3600);
                incr += getDiff(curs, s, 60, 1, 60);

                sleep(incr);
            }

            while(1) {
                iterTask(60, 60);
                sleep(82800);
            }

        }
        ```

    - Apabila input berupa `H M \*`, maka script bash akan dijalankan apabila jam terkini adalah H dengan menitnya adalah M. Untuk itu, akan dilakukan pengecekkan apakah jam dan menit terkini sudah sesuai dengan H dan M atau belum. Apabila sudah, maka akan dilakukan iterasi eksekusi setiap 1 detik hingga berganti menit. Bila belum, maka akan dilakukan sleep hingga jam ke-H dan menit ke-M yang terdekat. Kemudian kedua branch akan bergabung dan akan dilakukan iterasi yang sama dengan jeda 23 jam per iterasi

        ```c
        else if (h != STAR && m != STAR && s == STAR) {
            int incr = 0;
            if (h == curh && m == curm) {
                iterTask(60 - curs, 1);
                sleep(86340);

            } else {
                incr += getDiff(curh, h, 24, 3600, 0);
                incr += getDiff(curm, m, 60, 60, 3600);
                incr += getDiff(curs, 0, 60, 1, 60);

                sleep(incr);
            }

            while(1) {
                iterTask(60, 1);
                sleep(86340);
            }

        }
        ```

    - Apabila input berupa `H M S`, maka script bash akan dijalankan setiap H:M:S yang artinya hanya perlu dilakukan sleep hingga waktu tersebut sebelum script bash dijalankan sekali dan dilanjutkan dengan jeda 24 jam untuk setiap eksekusi

        ```c
        else {
            int incr = 0;
            incr += getDiff(curh, h, 24, 3600, 0);
            incr += getDiff(curm, m, 60, 60, 3600);
            incr += getDiff(curs, s, 60, 1, 60);

            sleep(incr);

            while(1) {
                execCmd();
                sleep(86400);
            }

        }
        ```

### Output

-   Contoh Run (`\* \* \*`):
    <img src="https://cdn.discordapp.com/attachments/995337235211763722/1090120380695449660/image.png">

-   Cek Process (ps aux):
    <img src="https://cdn.discordapp.com/attachments/995337235211763722/1090120490808508416/image.png">

-   Isi programcron.sh:

    ```bash
    echo "Programnya jalan bos." >> "/home/kev/Kuliah/Sisop/files2/result.txt"
    ```

-   Cek Hasil:

    <img src="https://cdn.discordapp.com/attachments/995337235211763722/1090120885723222146/image.png">

### Kendala yang sempat dihadapi

-   Sempat kebingungan terhadap perhitungan terhadap waktu sleep yang harus diberikan untuk setiap kasus yang memerlukan brainstorming dan waktu berpikir yang cukup lama
-   Sempat mengalami kebingungan juga terhadap branching apabila jam dan menit terkini sudah sesuai dengan yang diinputkan
