#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

void buatTim(int bek, int gelandang, int striker) {

    pid_t id1, id2, id3;
    int status;
    char kiperStr[200], bekStr[200], gelStr[200], strikerStr[200];

    sprintf(kiperStr, "ls Kiper/ | sort -r -t _ -k 4 | head -n 1 | cut -d . -f 1 > /home/$USER/Formasi_%d-%d-%d.txt", bek, gelandang, striker);
    sprintf(bekStr, "ls Bek/ | sort -r -t _ -k 4 | head -n %d | cut -d . -f 1 >> /home/$USER/Formasi_%d-%d-%d.txt", bek, bek, gelandang, striker);
    sprintf(gelStr, "ls Gelandang/ | sort -r -t _ -k 4 | head -n %d | cut -d . -f 1 >> /home/$USER/Formasi_%d-%d-%d.txt", gelandang, bek, gelandang, striker);
    sprintf(strikerStr, "ls Penyerang/ | sort -r -t _ -k 4 | head -n %d | cut -d . -f 1 >> /home/$USER/Formasi_%d-%d-%d.txt", striker, bek, gelandang, striker);
    
    id1 = fork();

    if (id1 < 0) {
        exit(EXIT_FAILURE);
    }

    else if (id1 == 0) {
        // add kiper player to formasi txt
        execlp("/bin/sh", "sh", "-c", kiperStr, NULL);
    }

    else {
        wait(&status);
        
        id2 = fork();

        if (id2 == 0) {
            // sort and add bek players to formasi txt
            execlp("/bin/sh", "sh", "-c", bekStr, NULL);
        }

        else {
            wait(&status);

            id3 = fork();

            if (id3 == 0) {
                // sort and add gelandang players to formasi txt
                execlp("/bin/sh", "sh", "-c", gelStr, NULL);
            }

            else {
                wait(&status);
                

                // sort and add striker players to formasi txt
                execlp("/bin/sh", "sh", "-c", strikerStr, NULL);
            }
        }
    }
}

int main() {
    
    pid_t id0, id1, id2, id3, id4, id5, id6;
    int status;

    id0 = fork();

    if(id0 < 0) {
        exit(EXIT_FAILURE);
    }

    else if(id0 == 0){
        
        id1 = fork();

        if (id1 < 0) {
            exit(EXIT_FAILURE);
        }

        else if (id1 == 0) {
            // download file player.zip
            printf("Downloading player.zip . . .\n");

            char *argv[] = {"wget", "-q", "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", "-O", "player.zip", NULL};
            execv("/bin/wget", argv);
        }

        else {
            wait(&status);

            id2 = fork();

            if (id2 == 0) {
                // extract player.zip
                printf("Unzipping player.zip\n");

                char *argv[] = {"unzip", "-q", "player.zip", NULL};
                execv("/bin/unzip", argv);
            }

            else {
                wait(&status);
                id3 = fork();

                if(id3 == 0){
                    // delete player.zip
                    printf("Deleting player.zip\n");

                    char *argv[] = {"rm", "player.zip", NULL};
                    execv("/bin/rm", argv);
                }

                else {
                    wait(&status);

                    id4 = fork();

                    if (id4 == 0) {
                        // delete non Manchester United players
                        printf("Deleting non Manchester United players\n");

                        char *argv[] = {"find", "players/", "-type", "f", "-not", "-name", "*ManUtd*", "-delete", NULL};
                        execv("/bin/find", argv);
                    }

                    else {
                        wait(&status);

                        char *folder[]={"Kiper", "Bek", "Gelandang", "Penyerang"};

                        for(int i=0; i<4; i++){
                            id5 = fork();

                            if(id5 == 0){
                                // copy players folder to each folder (Kiper, Bek, Gelandang, and Penyerang)
                                char *argv[] = {"cp", "-R", "players/", folder[i], NULL};
                                execv("/bin/cp", argv);
                            }
                        }

                        if(id5 != 0) {
                            wait(&status);

                            char *folder2[] = {"*Kiper*", "*Bek*", "*Gelandang*", "*Penyerang*"};

                            for(int i=0; i<4; i++){
                                id6 = fork();

                                if(id6 == 0){
                                    // filter players according to their folder
                                    char *argv[] = {"find", folder[i], "-type", "f", "-not", "-name", folder2[i], "-delete", NULL};
                                    execv("/bin/find", argv);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    else {
        wait(&status);

        int bek, gelandang, striker, count=1;

        while(count) {
            printf("\nMasukkan formasi pemain yang diinginkan: \n");
            printf("Bek: ");
            scanf("%d", &bek);
            printf("Gelandang: ");
            scanf("%d", &gelandang);
            printf("Striker: ");
            scanf("%d", &striker);

            if (bek == 0 || gelandang == 0 || striker == 0) {
                printf("Jumlah pemain tidak boleh 0! Silahkan coba lagi\n");
            }

            else if (bek + gelandang + striker < 10) {
                printf("Jumlah pemain kurang dari 11! Silahkan coba lagi\n");
            }

            else if (bek + gelandang + striker > 10) {
                printf("Jumlah pemain lebih dari 11! Silahkan coba lagi\n");
            }

            else {
                count = 0;
            }

        }
        printf("File Formasi_%d-%d-%d.txt telah dibuat di directory /home/%s/\n", bek, gelandang, striker, getlogin());


        buatTim(bek, gelandang, striker);
    }
}