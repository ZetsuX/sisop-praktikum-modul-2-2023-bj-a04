#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

#define MAX 10001
#define STAR 404

char path[MAX];
char *args[] = {"bash", path, NULL};

short validateTime(char* str, short max, char* name) {
    if (strlen(str) <= 0 || strlen(str) > 2) {
        printf("The entered %s is invalid! Please check and rerun the program with the correct %s.\n", name, name);
        return -1;
    }

    if (str[0] == '*') {
        if (strlen(str) == 1) {
            return STAR;
        } else {
            printf("The entered %s is invalid! Please check and rerun the program with the correct %s.\n", name, name);
            return -1;
        }
    }

    if (strlen(str) == 2) {
        if (!isdigit(str[0]) || !isdigit(str[1])) {
            printf("The entered %s is invalid! Please check and rerun the program with the correct %s.\n", name, name);
            return -1;
        }
    } else {
        if (!isdigit(str[0])) {
            printf("The entered %s is invalid! Please check and rerun the program with the correct %s.\n", name, name);
            return -1;
        }
    }
    
    int t = atoi(str);
    if (t < 0 || t > max) {
        printf("The entered %s is outside the range of 0 - %hi! Please check and rerun the program with the correct %s.\n", name, max, name);
        return -1;
    } 
    
    return t;
}

int getDiff(int cur, int t, int add, int mul, int subt) {
    if (cur < t) {
        return ((t - cur)*mul);
    } else if (cur > t) {
        return (((t + add - cur)*mul) - subt);
    }

    return 0;
}

void execCmd() {
    pid_t chId;
    chId = fork();

    if (chId < 0) {
        exit(EXIT_FAILURE);
    }

    if (chId == 0) {
        execv("/bin/bash", args);
    }
}

void iterTask(int max, int slp) {
    execCmd();
    for (int i = 1 ; i < max ; i++) {
        sleep(slp);
        execCmd();
    }
}

int main(int argc, char **argv) {
    if (argc != 5) {
        printf("The entered arguments are invalid! Please check and rerun the program with the fixed arguments.\n");
        return 0;
    }

    short h, m, s;

    h = validateTime(argv[1], 23, "hour");
    if (h == -1) return 0;

    m = validateTime(argv[2], 59, "minute");
    if (m == -1) return 0;

    s = validateTime(argv[3], 59, "second");
    if (s == -1) return 0;
    
    if (strlen(argv[4]) > MAX) {
        printf("The entered file path is too long. Please shorten it and then rerun the program with the shortened path.\n");
        return 0;
    }

    strcpy(args[1], argv[4]);

    pid_t pid, sid;
    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    printf("Daemon Process created successfully! (PID : %d)\n", sid);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    time_t nowTime = time(NULL);
    struct tm *tm_struct = localtime(&nowTime);

    int curh = tm_struct->tm_hour;
    int curm = tm_struct->tm_min;
    int curs = tm_struct->tm_sec;

    if (h == STAR && m == STAR && s == STAR) {
        while (1) {
            execCmd();
            sleep(1);
        }

    } else if (h == STAR && m == STAR && s != STAR) {
        int incr = 0;
        incr += getDiff(curs, s, 60, 1, 0);

        sleep(incr);
        execCmd();

        while (1) {
            sleep(60);
            execCmd();
        }

    } else if (h == STAR && m != STAR && s == STAR) {
        int incr = 0;
        incr += getDiff(curm, m, 60, 60, 0);
        incr += getDiff(curs, 0, 60, 1, 60);

        sleep(incr);

        while (1) {
            iterTask(60, 1);
            sleep(3540); // tiap 1 jam - 1 menit
        }

    } else if (h != STAR && m == STAR && s == STAR) {
        int incr = 0;
        if (h == curh) {
            incr += getDiff(curm, 59, 60, 60, 0);
            incr += getDiff(curs, 59, 60, 1, 60);

            iterTask(incr + 1, 1);
            sleep(82800); // tiap 24 jam - 1 jam

        } else {
            incr += getDiff(curh, h, 24, 3600, 0);
            incr += getDiff(curm, 0, 60, 60, 3600);
            incr += getDiff(curs, 0, 60, 1, 60);

            sleep(incr);
        }

        while (1) {
            iterTask(3600, 1);
            sleep(82800); // tiap 24 jam - 1 jam
        }

    } else if (h == STAR && m != STAR && s != STAR) {
        int incr = 0;
        incr += getDiff(curm, m, 60, 60, 0);
        incr += getDiff(curs, s, 60, 1, 60);

        sleep(incr);

        while(1) {
            execCmd();
            sleep(3600);
        }

    } else if (h != STAR && m == STAR && s != STAR) {
        int incr = 0;
        if (h == curh) {
            if (curm < 59 || (curm == 59 && curs <= s)) {
                incr += getDiff(curs, s, 60, 1, 0);
                sleep(incr);

                iterTask(60 - curm, 60);
                sleep(82800); // tiap 24 jam - 1 jam
            }
        } else {
            incr += getDiff(curh, h, 24, 3600, 0);
            incr += getDiff(curm, 0, 60, 60, 3600);
            incr += getDiff(curs, s, 60, 1, 60);

            sleep(incr);
        }

        while(1) {
            iterTask(60, 60);
            sleep(82800); // tiap 24 jam - 1 jam
        }

    } else if (h != STAR && m != STAR && s == STAR) {
        int incr = 0;
        if (h == curh && m == curm) {
            iterTask(60 - curs, 1);
            sleep(86340); // tiap 24 jam - 1 menit

        } else {
            incr += getDiff(curh, h, 24, 3600, 0);
            incr += getDiff(curm, m, 60, 60, 3600);
            incr += getDiff(curs, 0, 60, 1, 60);

            sleep(incr);
        }

        while(1) {
            iterTask(60, 1);
            sleep(86340); // tiap 24 jam - 1 menit
        }

    } else {
        int incr = 0;
        incr += getDiff(curh, h, 24, 3600, 0);
        incr += getDiff(curm, m, 60, 60, 3600);
        incr += getDiff(curs, s, 60, 1, 60);

        sleep(incr);

        while(1) {
            execCmd();
            sleep(86400);
        }

    }
}